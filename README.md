## Func
**_A dead simple AD633-based multiplier and divider. For LW._**

Func is a basic AM-modulator based on a four quadrant multiplier chip, the AD633. This (rather expensive) little chip is quite easy to use and sounds great.
This implementation features:
* Audio input (**Carrier**) with attenuation control
* **Modulator** inputs, simultaneously DC-coupled and AC-coupled with attenuation
* Voltage-controlled offset with attenuation
* Multiply/divide modes: the first is smooth AM, the second is harsh distortion
* Initial gain (**Init**) to mimick a VCA

This module was designed to overdrive quite easily the signal with softclip.
It has nothing new, or nothing exciting, but it works enough for my purposes. It was a small project I started because I had some AD633s around.

![alt text](pics/front.JPG "explainations")

Despite the simplicity of the schematics, the module packs many functions:
* Amplitude-modulation sounds (duh)
* Crude VCA with initial gain control
* Audio attenuator/inverter
* Distortion, from subtle overdrive to harder clipping
* Basic 2-inputs mixer using the offset input
* DC generator (around +/-5V)
* Waveshaper with functions such as squaring, etc
* Great for tremolo effects
* ... and some of these simultaneously.

It is not a complex build, with really basic and inexpensive (except for the AD633) components. It requires a bit of wiring for the switch only.

Be careful: the SMD AD633 **does not have** the same pinout as the DIP version. If using an adapter for this project, you'll have to make them yourself!

### Repository contents:
- The hardware files:
    - The whole KiCad project
    - The Bill of Materials (BoM)
    - The gerber files
    - A pdf export of the schematics
- Panel designs:
    - The SVG for the panel design
    - The exported pdf

The project is confirmed working. If you are interested in this project, you can order boards yourself from the files in **hardware/version_2/fab**.
